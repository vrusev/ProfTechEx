﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BikeSales.Models;
using Microsoft.AspNetCore.Cors;
using System.Runtime.CompilerServices;
using Microsoft.Identity.Client;

namespace BikeSales.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class BikeSalesController : ControllerBase
    {

        private readonly SalesDbContext _context;

        public BikeSalesController(SalesDbContext context)
        {
            _context = context;
        }

        #region - Helpers - 

        private async Task<IActionResult> SaveHandle()
        {
            try
            {
                await _context.SaveChangesAsync();
                return StatusCode(StatusCodes.Status200OK, "{status: success}");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }

        #endregion

        #region - HttpPost -

        [HttpPost]
        public async Task<IActionResult> NewSaleRecord(Sales sale)
        {
            if (String.IsNullOrEmpty(sale.ProductName) ||
               String.IsNullOrEmpty(sale.CustomerName))
            {
                return BadRequest();
            }

            _context.Attach(sale);
            _context.Entry(sale).State = EntityState.Added;

            return await SaveHandle();
        }

        #endregion

        #region - HttpPut - 

        [HttpPut]
        public async Task<IActionResult> UpdateProductRecord(string productName, Product product)
        {
            if (productName != product.ProductName )
            {
                return BadRequest();
            }

            _context.Product.Attach(product);
            _context.Entry(product).State = EntityState.Modified;

            return await SaveHandle();
        }

        [HttpPut]
        public async Task<IActionResult> UpdateSalespersonRecord(string salespersonName, Salesperson salesperson)
        {
            if (!salespersonName.Contains(salesperson.FirstName) ||
                !salespersonName.Contains(salesperson.LastName))
            {
                return BadRequest();
            }

            _context.Salesperson.Attach(salesperson);
            _context.Entry(salesperson).State = EntityState.Modified;

            return await SaveHandle();
        }

        #endregion

        #region - HttpGet -

        [HttpGet]       
        public async Task<IActionResult> GetSalespersonsList()
        {
            if (_context.Salesperson == null)
            {
                return NotFound();
            }

            var result = await _context.Salesperson.ToListAsync();
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetProductsList()
        {
            if (_context.Product == null)
            {
                return NotFound();
            }

            var result = await _context.Product.ToListAsync();
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetCustomersList()
        {
            if (_context.Customer == null)
            {
                return NotFound();
            }

            var result = await _context.Customer.ToListAsync();
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetDiscountsList()
        {
            if (_context.Discount == null)
            {
                return NotFound();
            }

            var result = await _context.Discount.ToListAsync();
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetSalesList()
        {
            if (_context.Sales == null || _context.Product == null)
            {
                return NotFound();
            }

            var query = from sales in _context.Sales
                        join product in _context.Product
                            on sales.ProductName equals product.ProductName
                            into allSales
                        from all in allSales.DefaultIfEmpty()
                        orderby sales.SalesDate                        
                        select new
                        {
                            sales.ProductName, 
                            sales.CustomerName, 
                            sales.SalesDate,
                            sales.Salesperson,                            
                            all.PurchasePrice,                            
                            all.CommissionPercentage
                        };                        

            var result = await query.ToListAsync();
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetCommissionsList(string year)
        {
            if (_context.Sales == null ||
                _context.Discount == null || 
                _context.Product == null)
            {
                return NotFound();
            }

            if (year == null)
            {
                return BadRequest();
            }

            var query = from sales in _context.Sales
                        join product in _context.Product
                            on sales.ProductName equals product.ProductName                                
                            into allSales                        
                        from sell in allSales.DefaultIfEmpty()
                            where sales.SalesDate.Contains(year)
                        orderby sales.Salesperson ascending
                        join discount in _context.Discount
                            on sales.ProductName equals discount.ProductName
                            into allDiscounts
                        from disc in allDiscounts.DefaultIfEmpty()
                            where sales.SalesDate.Contains(year)
                        orderby sales.Salesperson ascending
                        select new
                        {
                            sales.ProductName,
                            sales.CustomerName,
                            sales.SalesDate,
                            sales.Salesperson,
                            sell.PurchasePrice,
                            sell.CommissionPercentage,
                            disc.DiscountPercentage
                        };

            var result = await query.ToListAsync();
            return Ok(result);
        }

        #endregion
    }
}
