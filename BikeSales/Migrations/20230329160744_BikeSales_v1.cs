﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BikeSales.Migrations
{
    /// <inheritdoc />
    public partial class BikeSales_v1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    FirstName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(17)", maxLength: 17, nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    StartDate = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => new { x.FirstName, x.LastName, x.Phone });
                });

            migrationBuilder.CreateTable(
                name: "Discount",
                columns: table => new
                {
                    ProductName = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    BeginDate = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    EndDate = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    DiscountPercentage = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Discount", x => new { x.ProductName, x.BeginDate, x.EndDate });
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    ProductName = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Manufacturer = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Style = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    PurchasePrice = table.Column<int>(type: "int", nullable: false),
                    SalesPrice = table.Column<int>(type: "int", nullable: false),
                    QtyOnHand = table.Column<int>(type: "int", nullable: false),
                    CommissionPercentage = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => new { x.ProductName, x.Manufacturer, x.Style });
                });

            migrationBuilder.CreateTable(
                name: "Sales",
                columns: table => new
                {
                    ProductName = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    CustomerName = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: false),
                    SalesDate = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    Salesperson = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sales", x => new { x.ProductName, x.CustomerName, x.SalesDate });
                });

            migrationBuilder.CreateTable(
                name: "Salesperson",
                columns: table => new
                {
                    FirstName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(17)", maxLength: 17, nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TerminationDate = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    Manager = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Salesperson", x => new { x.FirstName, x.LastName, x.Phone });
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropTable(
                name: "Discount");

            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "Sales");

            migrationBuilder.DropTable(
                name: "Salesperson");
        }
    }
}
