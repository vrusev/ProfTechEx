﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BikeSales.Migrations
{
    /// <inheritdoc />
    public partial class BikeSales_v2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "StartDate",
                table: "Salesperson",
                type: "nvarchar(64)",
                maxLength: 64,
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StartDate",
                table: "Salesperson");
        }
    }
}
