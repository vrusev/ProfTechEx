﻿using System.ComponentModel.DataAnnotations;

namespace BikeSales.Models;

public class Discount
{
    [Required]
    [Key]
    public string ProductName { get; set; } = null!;

    [Required]
    [MaxLength(64)]
    public string BeginDate { get; set; } = null!;

    [Required]
    [MaxLength(64)]
    public string EndDate { get; set; } = null!;

    public int DiscountPercentage { get; set; }
}