﻿using System.ComponentModel.DataAnnotations;

namespace BikeSales.Models;

public class Product
{
    [Required]
    public string ProductName { get; set; } = null!;

    [Required]
    public string Manufacturer { get; set; } = null!;

    [Required]
    public string Style { get; set; } = null!;

    [Required]
    public int PurchasePrice { get; set; }

    [Required]
    public int SalesPrice { get; set; }

    public int QtyOnHand { get; set; }

    [Required]
    public int CommissionPercentage { get; set; }
}