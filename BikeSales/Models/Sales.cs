﻿using System.ComponentModel.DataAnnotations;

namespace BikeSales.Models;

public class Sales
{
    [Required]
    public string ProductName { get; set; } = null!;

    [Required]
    [StringLength(40)]
    public string Salesperson { get; set; } = null!;

    [Required]
    [StringLength(40)]
    public string CustomerName { get; set; } = null!;

    [Required]
    [MaxLength(64)]
    public string SalesDate { get; set; } = null!;
}
