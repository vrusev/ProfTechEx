﻿using Microsoft.EntityFrameworkCore;

namespace BikeSales.Models;

public class SalesDbContext : DbContext
{
    private IConfiguration? Configuration;
    public SalesDbContext(IConfiguration configuration)
    {
        Configuration = configuration;
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer(Configuration?.GetConnectionString("BikeSales")); //(Configuration?["BikeSales"]);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Customer>()
            .HasKey(nameof(Models.Customer.FirstName), nameof(Models.Customer.LastName), nameof(Models.Customer.Phone));

        modelBuilder.Entity<Salesperson>()
            .HasKey(nameof(Models.Salesperson.FirstName), nameof(Models.Salesperson.LastName), nameof(Models.Salesperson.Phone));

        modelBuilder.Entity<Product>()
            .HasKey(nameof(Models.Product.ProductName), nameof(Models.Product.Manufacturer), nameof(Models.Product.Style));

        modelBuilder.Entity<Sales>()
            .HasKey(nameof(Models.Sales.ProductName), nameof(Models.Sales.CustomerName), nameof(Models.Sales.SalesDate));

        modelBuilder.Entity<Discount>()
            .HasKey(nameof(Models.Discount.ProductName), nameof(Models.Discount.BeginDate), nameof(Models.Discount.EndDate));
    
        //Note: Deriving Salesperson from Customer does not create table
    }

    public DbSet<Customer> Customer { get; set; } = null!;
    public DbSet<Product> Product { get; set; } = null!;
    public DbSet<Salesperson> Salesperson { get; set; } = null!;
    public DbSet<Sales> Sales { get; set; } = null!;
    public DbSet<Discount> Discount { get; set; } = null!;
}
