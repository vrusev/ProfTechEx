﻿using System.ComponentModel.DataAnnotations;

namespace BikeSales.Models;

public class Salesperson
{
    [Required]
    [MaxLength(20)]
    public string FirstName { get; set; } = null!;

    [Required]
    [MaxLength(20)]
    public string LastName { get; set; } = null!;

    [Required]
    public string Address { get; set; } = null!;

    [Required]
    [MaxLength(17)]
    public string Phone { get; set; } = null!;

    [Required]
    [MaxLength(64)]
    public string StartDate { get; set; } = null!;

    [Required]
    [MaxLength(64)]
    public string TerminationDate { get; set; } = null!;

    [Required]
    [MaxLength(40)]
    public string Manager { get; set; } = null!;
}