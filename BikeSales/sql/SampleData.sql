USE [BikeSales]
GO

------------------------- !TRUNCATE! -------------------------

truncate table [dbo].[Customer]
truncate table [dbo].[Discount]
truncate table [dbo].[Product]
truncate table [dbo].[Sales]
truncate table [dbo].[Salesperson]

------------------------- Customers -----------------------------

insert into [dbo].[Customer]
values ('Joe', 'Ryder', '(678) 345 - 2232', '322 Feathers Dr., Douglasville GA', '08/12/1999')
GO

insert into [dbo].[Customer]
values ('Marry', 'West', '(678) 483 - 0044', '39213 Unity Ln., Marietta GA', '06/07/2010')
GO

insert into [dbo].[Customer]
values ('John', 'Dilland', '(770) 098 - 4825', '111 Westward Dr., Atlanta GA', '08/30/2006')
GO

insert into [dbo].[Customer]
values ('Sara', 'Fior', '(413) 499 - 4141', '4970 Sunny Ave., Springfield MA', '02/10/2023')
GO

---------------------------- Discounts ----------------------------

insert into [dbo].[Discount]
values ('Bakcou Flatlander', '01/01/2023', '04/01/2023', 5)
GO

insert into [dbo].[Discount]
values ('BMC Speedfox', '02/15/2023', '04/30/2023', 5)
GO

insert into [dbo].[Discount]
values ('Diamondback Overdrive', '03/30/2023', '06/01/2023', 7)
GO

insert into [dbo].[Discount]
values ('Giant Reign', '03/01/2023', '12/30/2023', 10)
GO

---------------------------- Products ----------------------------

insert into [dbo].[Product]
values ('Flatlander', 'Bakcou', 'Trail', 1200, 1140, 3, 17)
GO

insert into [dbo].[Product]
values ('Speedfox', 'BMC', 'Cross country', 1000, 950, 2, 10)
GO

insert into [dbo].[Product]
values ('Overdrive', 'Diamondback', 'Hardtails', 1400, 1302, 4, 8)
GO

insert into [dbo].[Product]
values ('Mission', 'Diamondback', 'Full Suspension', 750, 750, 4, 10)
GO

insert into [dbo].[Product]
values ('Reign', 'Giant', 'Electric', 1200, 1080, 3, 12)
GO

insert into [dbo].[Product]
values ('Horsethief', 'Salsa Cycles', 'Trail', 1200, 1140, 11, 5)
GO

insert into [dbo].[Product]
values ('Rustler', 'Salsa Cycles', 'Cross country', 1700, 1700, 2, 17)
GO
---------------------------- Sales ----------------------------

insert into [dbo].[Sales]
values ('Backou Flatlander', 'Joe Soyer', '03/31/2023', 'Jensen Wheeler')
GO

insert into [dbo].[Sales]
values ('Giant Reign', 'Martha Green', '04/30/2022', 'Jensen Wheeler')
GO

insert into [dbo].[Sales]
values ('Diamondback Mission', 'Michael Russo', '04/12/2020', 'Jerry Watson')
GO

---------------------------- Salespersons ----------------------------

insert into [dbo].[Salesperson]
values ('Jensen', 'Wheeler', '(770) 482 - 7719', '1 Forrest Dr., Gainsville GA', '', 'Stuart Boggs', '12/03/2020')
GO

insert into [dbo].[Salesperson]
values ('Anna', 'Smith', '(770) 293 - 0293', '1201 Fencing St., Gainsville GA', '', 'Stuart Boggs', '10/28/2022')
GO

insert into [dbo].[Salesperson]
values ('Jerry', 'Watson', '(678) 440 - 4012', '1201 Lumber Ln., Atlanta GA', '', 'Jane Westley', '01/22/2009')
GO