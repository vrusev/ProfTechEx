//import logo from './logo.svg';
import './App.css';
import {Salespersons} from './Salespersons'
import {BrowserRouter, Route, Routes, NavLink} from 'react-router-dom'

function App() {
  return (
    <BrowserRouter>
    <div>
      <h4>
        Bike Sales
      </h4>

      <nav>
        <ul className='navbar-nav'>
          <li className='m-1'>
            <NavLink className="btn btn-light" to="/salespersons">
              Salespersons
            </NavLink>  
          </li>          
        </ul>
      </nav>

      <Routes>
        <Route path='/salespersons' component={<Salespersons/>}/> 
      </Routes>
    </div>
    </BrowserRouter>
  );
}

export default App;
