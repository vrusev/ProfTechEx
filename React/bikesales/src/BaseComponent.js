import {Component} from 'react'

export class BaseComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            records: [],
            error: null,
            isLoaded: false
        };
    }

    componentDidMount() {
                  
    }

    async manageData (url = "", m = "", b = ""){
        var response;
        var header = {
            'Content-Type': 'application/json'
        };

        if(m === 'GET'){
            response = await fetch(url, {
                method: m,
                headers: header
            });
        } else if (b !== '') {
            response = await fetch(url, {
                method: m,
                headers: header,
                body: JSON.stringify(b)
            });
        }

        return await response.json();
    }

    render() {
        return;
    }
}