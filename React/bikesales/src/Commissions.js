import React from 'react'
import {APIEndpoints} from "./Constants"
import { BaseComponent } from './BaseComponent';

export class Commissions extends BaseComponent {
    constructor(props) {
        super(props);

        var currentYear = new Date().getFullYear();
        var years = [];
        for(var i = 1999; i <=  currentYear; i ++){
            years.push(i);
        }

        this.state = {
            records: [],
            error: null,
            isLoaded: false,
            years
        };
    }

    jsfCalculateCommissions(data){
        var records = [];
        var uniqueNames = [];    
        data.forEach(item => {
            if(uniqueNames.indexOf(item.salesperson) < 0)
                uniqueNames.push(item.salesperson);

            var record = {salesperson: item.salesperson, q1: 0, q2: 0, q3: 0, q4: 0};
            var month = new Date(item.salesDate).getMonth();
            
            var afterDiscount = item.purchasePrice - ((item.purchasePrice/100) * item.discountPercentage);
            var commission = Math.round((afterDiscount/100) * item.commissionPercentage);

            if(month <= 3){
                record.q1 = commission;
            } else if (month <= 6){
                record.q2 = commission;
            } else if (month <= 9){
                record.q3 = commission;
            } else {
                record.q4 = commission;
            }

            records.push(record);
        });
        
        return {quarterlyRecords: records, names: uniqueNames};
    }

    jsfGetCommissions(data){
        var obj = this.jsfCalculateCommissions(data);
        var records = [];

        obj.names.forEach(name => {
            var record = {salesperson: name, q1: 0, q2: 0, q3: 0, q4: 0};                        
            obj.quarterlyRecords.forEach(item => {
                if(name === item.salesperson){
                    if(!isNaN(item.q1)){
                        record.q1 = record.q1 + item.q1;
                    } 

                    if(!isNaN(item.q2)){
                        record.q2 = record.q2 + item.q2;
                    }

                    if(!isNaN(item.q3)){
                        record.q3 = record.q3 + item.q3;
                    }

                    if(!isNaN(item.q4)){
                        record.q4 = record.q4 + item.q4;
                    }
                }                
            });

            records.push(record);
        });

        return records;
    }

    componentDidMount() {
        this.interval = setInterval(() => {
            var select = document.getElementById("yearSelect");            
            var year = (select != null ? select.options[select.selectedIndex].value : "2022");
            
            this.manageData(APIEndpoints.base_url + APIEndpoints.commissions + year, 'GET', '')
            .then((data) => {
                var dt = this.jsfGetCommissions(data);
                this.setState({ records: dt, isLoaded: true });            
            })
            .catch((err) => {
                this.setState({ error: err, isLoaded: true });
                console.log(err);
            });
        }, 4000);         
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        const { records, error, isLoaded, years } = this.state;        
        if (!error && isLoaded) {
            return (
                <div id={"containers"}>
                    <select id="yearSelect">
                        {years.map(year => (
                            <option key={year} value={year}>{year}</option>
                        ))}                        
                    </select>
                    <table id={"views"}>
                        <thead>
                            <tr>
                                <td>Salesperson</td><td>Q1</td><td>Q2</td><td>Q3</td><td>Q4</td>
                            </tr>
                        </thead>
                        <tbody>
                            {records.map((record, idx) => (                        
                                <tr key={"row_" + idx}>
                                    <td><label key={"salesperson_" + idx}>{record.salesperson}</label></td>                                
                                    <td><label key={"q1_" + idx}>{'$' + record.q1}</label></td>
                                    <td><label key={"q2_" + idx}>{'$' + record.q2}</label></td>                                
                                    <td><label key={"q3_" + idx}>{'$' + record.q3}</label></td>
                                    <td><label key={"q4_" + idx}>{'$' + record.q4}</label></td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            );
        } else if (!isLoaded) {
            return <div>Loading, please wait...</div>;
        } else {
            return <div>Error: error</div>;
        }
    }
}