export const APIEndpoints = {
    base_url: "http://localhost:5291/api/BikeSales/",
    salespersons: "GetSalespersonsList",
    customers: "GetCustomersList",
    products: "GetProductsList",
    discounts: "GetDiscountsList",
    sales: "GetSalesList",
    update_product: "UpdateProductRecord?productName=",
    update_salesperson: "UpdateSalespersonRecord?salespersonName=",
    new_sale: "NewSaleRecord",
    commissions: "GetCommissionsList?year="
}