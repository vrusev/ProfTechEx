import React from 'react'
import {APIEndpoints} from "./Constants"
import { BaseComponent } from './BaseComponent';

export class Customers extends BaseComponent {
    constructor(props) {
        super(props);

        this.state = {
            records: [],
            error: null,
            isLoaded: false
        };
    }

    componentDidMount() {
        this.manageData(APIEndpoints.base_url + APIEndpoints.customers, 'GET', '')
        .then((data) => {
            this.setState({ records: data, isLoaded: true });            
        })
        .catch((err) => {
            this.setState({ error: err, isLoaded: true });
            console.log(err);
        });            
    }

    render() {
        const { records, error, isLoaded } = this.state;
        if (!error && isLoaded) {
            return (
                <div id={"containers"}>
                    <table id={"views"}>
                        <thead>
                            <tr>
                                <td>First Name</td><td>Last Name</td><td>Address</td><td>Phone</td>
                                <td>Start Date</td>
                            </tr>
                        </thead>
                        <tbody>
                            {records.map((record, idx) => (                        
                                <tr key={"row_" + idx}>
                                    <td><label key={"firstName_" + idx}>{record.firstName}</label></td>
                                    <td><label key={"lastName_" + idx}>{record.lastName}</label></td>
                                    <td><label key={"address_" + idx}>{record.address}</label></td>
                                    <td style={{textAlign:'right'}}><label key={"phone_" + idx}>{record.phone}</label></td>
                                    <td><label key={"startDate_" + idx}>{record.startDate}</label></td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            );
        } else if (!isLoaded) {
            return <div>Loading, please wait...</div>;
        } else {
            return <div>Error: error</div>;
        }
    }
}