import React from 'react'
import {APIEndpoints} from "./Constants"
import { BaseComponent } from './BaseComponent';

export class NewSale extends BaseComponent {
    constructor(props) {
        super(props);

        this.state = {
            records: {},
            error: null,
            isLoaded: false
        };
    }

    componentDidMount() {
        this.setState({ records: {
                productName: "",
                salesperson: "",
                customerName: "",
                salesDate: ""
            },
            isLoaded: true 
        });                       
    }

    jsfUpdateSaleJson(event){
        if(event.target){
            var id = event.target.id;                                
            var data = this.state.records;
            var val = event.target.value;
            
            switch(id){
                case "productName":
                    data.productName = val;
                    break;
                case "salesperson":
                    data.salesperson = val;
                    break;
                case "customerName":
                    data.customerName = val;
                    break;
                case "salesDate":
                    data.salesDate = val;
                    break;
                default:
                    break;
            }

            this.setState({records: data});
        }
    }

    jsfNewSale(event){
        if(event.target){
            var data = this.state.records;

            this.manageData(APIEndpoints.base_url + APIEndpoints.new_sale, 'POST', data)
            .then((data) => {
                this.setState({ records: {data} });
            })
            .catch((err) => {
                console.log(err);
            });
        }
    }

    render() {
        const { records, error, isLoaded } = this.state;
        if (!error && isLoaded) {
            return (
                <table>
                    <thead>
                        <tr>
                            <td>Product Name</td><td>Salesperson</td><td>Customer</td><td>Sale Date</td>
                            <td>Event</td>
                        </tr>
                    </thead>
                    <tbody>                                               
                        <tr key={"row_0"}>
                            <td><input id={"productName"} key={"productName"} onChange={this.jsfUpdateSaleJson.bind(this)}></input></td>
                            <td><input id={"salesperson"} key={"salesperson"} onChange={this.jsfUpdateSaleJson.bind(this)}></input></td>
                            <td><input id={"customerName"} key={"customerName"} onChange={this.jsfUpdateSaleJson.bind(this)}></input></td>
                            <td><input id={"salesDate"} key={"salesDate"} onChange={this.jsfUpdateSaleJson.bind(this)}></input></td>
                            <td><button id={"save"} key={"save"} onClick={this.jsfNewSale.bind(this)}>Save</button></td>
                        </tr>                        
                    </tbody>
                </table>
            );
        } else if (!isLoaded) {
            return <div>Loading, please wait...</div>;
        } else {
            return <div>Error: error</div>;
        }
    }
}