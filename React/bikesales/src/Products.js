import React from 'react'
import {APIEndpoints} from "./Constants"
import { BaseComponent } from './BaseComponent';

export class Products extends BaseComponent {
    constructor(props) {
        super(props);

        this.state = {
            records: [],
            error: null,
            isLoaded: false
        };
    }

    componentDidMount() {
        this.manageData(APIEndpoints.base_url + APIEndpoints.products, 'GET', '')
        .then((data) => {
            this.setState({ records: data, isLoaded: true });            
        })
        .catch((err) => {
            this.setState({ error: err, isLoaded: true });
            console.log(err);
        });            
    }

    jsfUpdateProductJson(event){
        if(event.target){
            var arr = event.target.id.split("_");                                
            var data = this.state.records;
            var val = event.target.value;
            
            switch(arr[0]){
                /*case "manufacturer":
                    data[arr[1]].manufecturer = val;
                    break;
                case "style":
                    data[arr[1]].style = val;
                    break;*/
                case "purchasePrice":
                    data[arr[1]].purchasePrice = val;
                    break;
                case "salesPrice":
                    data[arr[1]].salesPrice = val;
                    break;
                case "qtyOnHand":
                    data[arr[1]].qtyOnHand = val;
                    break;
                case "commissionPercentage":
                    data[arr[1]].commissionPercentage = val;
                    break;
                default:
                    break;
            }

            this.setState({records: data});
        }
    }

    jsfUpdateProduct(event){
        if(event.target){
            var arr = event.target.id.split("_");
            var data = this.state.records[arr[1]];

            this.manageData(APIEndpoints.base_url + APIEndpoints.update_product + data.productName, 'PUT', data)
            .then((data) => {
                this.setState({ records: data });            
            })
            .catch((err) => {
                console.log(err);
            }); 
        }
    }

    render() {
        const { records, error, isLoaded } = this.state;
        if (!error && isLoaded) {
            return (
                <table>
                    <thead>
                        <tr>
                            <td>Name</td><td>Manufacturer</td><td>Style</td><td>Purchase Price</td>
                            <td>Sales Price</td><td>Qty On Hand</td><td>Commission Percentage</td><td>Event</td>
                        </tr>
                    </thead>
                    <tbody>
                        {records.map((record, idx) => (                        
                            <tr key={"row_" + idx}>
                                <td><label key={"name_" + idx}>{record.productName}</label></td>
                                <td><input readOnly={true} id={"manufacturer_" + idx} key={"manufacturer_" + idx} value={record.manufacturer} onChange={this.jsfUpdateProductJson.bind(this)}></input></td>
                                <td><input readOnly={true} id={"style_" + idx} key={"style_" + idx} value={record.style} onChange={this.jsfUpdateProductJson.bind(this)}></input></td>
                                <td><input id={"purchasePrice_" + idx} key={"purchasePrice_" + idx} value={record.purchasePrice} onChange={this.jsfUpdateProductJson.bind(this)}></input></td>
                                <td><input id={"salesPrice_" + idx} key={"salesPrice_" + idx} value={record.salesPrice} onChange={this.jsfUpdateProductJson.bind(this)}></input></td>
                                <td><input id={"qtyOnHand_" + idx} key={"qtyOnHand_" + idx} value={record.qtyOnHand} onChange={this.jsfUpdateProductJson.bind(this)}></input></td>
                                <td><input id={"commissionPercentage_" + idx} key={"commissionPercentage_" + idx} value={record.commissionPercentage} onChange={this.jsfUpdateProductJson.bind(this)}></input></td>
                                <td><button id={"save_" + idx} key={"save_" + idx} onClick={this.jsfUpdateProduct.bind(this)}>Save</button></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else if (!isLoaded) {
            return <div>Loading, please wait...</div>;
        } else {
            return <div>Error: error</div>;
        }
    }
}