import React from 'react'
import {APIEndpoints} from "./Constants"
import { BaseComponent } from './BaseComponent';

export class Sales extends BaseComponent {
    constructor(props) {
        super(props);

        this.state = {
            records: [],
            error: null,
            isLoaded: false
        };
    }

    componentDidMount() {        
        this.interval = setInterval(() => {
            this.manageData(APIEndpoints.base_url + APIEndpoints.sales, 'GET', '')
            .then((data) => {
                this.setState({ records: data, isLoaded: true });            
            })
            .catch((err) => {
                this.setState({ error: err, isLoaded: true });
                console.log(err);
            });
        }, 4000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        const { records, error, isLoaded } = this.state;
        if (!error && isLoaded) {
            return (
                <div id={"containers"}>
                    <table id={"views"}>
                        <thead>
                            <tr>
                                <td>Product Name</td><td>Customer</td><td>Sale Date</td>
                                <td>Salesperson</td><td>Price</td><td>Salesperson Commission</td>
                            </tr>
                        </thead>
                        <tbody>
                            {records.map((record, idx) => (                        
                                <tr key={"row_" + idx}>
                                    <td><label key={"productName_" + idx}>{record.productName}</label></td>                                
                                    <td><label key={"customerName_" + idx}>{record.customerName}</label></td>
                                    <td><label key={"salesDate_" + idx}>{record.salesDate}</label></td>                                
                                    <td><label key={"salesperson_" + idx}>{record.salesperson}</label></td>
                                    <td><label key={"purchasePrice_" + idx}>{'$' + record.purchasePrice}</label></td>
                                    <td><label key={"commissionPercentage_" + idx}>{record.commissionPercentage + '%'}</label></td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            );
        } else if (!isLoaded) {
            return <div>Loading, please wait...</div>;
        } else {
            return <div>Error: error</div>;
        }
    }
}