import React from 'react'
import {APIEndpoints} from "./Constants"
import { BaseComponent } from './BaseComponent';

export class Salespersons extends BaseComponent {
    constructor(props) {
        super(props);

        this.state = {
            records: [],
            error: null,
            isLoaded: false
        };
    }

    componentDidMount() {
        this.manageData(APIEndpoints.base_url + APIEndpoints.salespersons, 'GET', '')
        .then((data) => {
            this.setState({ records: data, isLoaded: true });            
        })
        .catch((err) => {
            this.setState({ error: err, isLoaded: true });
            console.log(err);
        });            
    }

    jsfUpdateSalespersonJson(event){
        if(event.target){
            var arr = event.target.id.split("_");                                
            var data = this.state.records;
            var val = event.target.value;
            
            switch(arr[0]){
                //case "phone":
                //    data[arr[1]].phone = val;
                //    break;
                case "address":
                    data[arr[1]].address = val;
                    break;                
                case "startDate":
                    data[arr[1]].startDate = val;
                    break;
                case "terminationDate":
                    data[arr[1]].terminationDate = val;
                    break;
                case "manager":
                    data[arr[1]].manager = val;
                    break;
                default:
                    break;
            }

            this.setState({records: data});
        }
    }

    jsfUpdateSalesPerson(event){
        if(event.target){
            var arr = event.target.id.split("_");
            var data = this.state.records[arr[1]];

            var parm = data.firstName + data.lastName
            this.manageData(APIEndpoints.base_url + APIEndpoints.update_salesperson + parm, 'PUT', data)
            .then((data) => {
                this.setState({ records: data });            
            })
            .catch((err) => {
                console.log(err);
            }); 
        }
    }

    render() {
        const { records, error, isLoaded } = this.state;
        if (!error && isLoaded) {
            return (
                <table>
                    <thead>
                        <tr>
                            <td>Name</td><td>Phone</td><td>Address</td><td>Start Date</td>
                            <td>Termination Date</td><td>Manager</td><td>Event</td>
                        </tr>
                    </thead>
                    <tbody>
                        {records.map((record, idx) => (                        
                            <tr key={"row_" + idx}>
                                <td><label key={"name_" + idx}>{record.firstName} {record.lastName}</label></td>
                                <td><input readOnly={true} id={"phone_" + idx} key={"phone_" + idx} value={record.phone} onChange={this.jsfUpdateSalespersonJson.bind(this)}></input></td>
                                <td><input id={"address_" + idx} key={"address_" + idx} value={record.address} onChange={this.jsfUpdateSalespersonJson.bind(this)}></input></td>
                                <td><input id={"startDate_" + idx} key={"startDate_" + idx} value={record.startDate} onChange={this.jsfUpdateSalespersonJson.bind(this)}></input></td>
                                <td><input id={"terminationDate_" + idx} key={"terminationDate_" + idx} value={record.terminationDate} onChange={this.jsfUpdateSalespersonJson.bind(this)}></input></td>
                                <td><input id={"manager_" + idx} key={"manager_" + idx} value={record.manager} onChange={this.jsfUpdateSalespersonJson.bind(this)}></input></td>
                                <td><button id={"save_" + idx} key={"save_" + idx} onClick={this.jsfUpdateSalesPerson.bind(this)}>Save</button></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            );
        } else if (!isLoaded) {
            return <div>Loading, please wait...</div>;
        } else {
            return <div>Error: error</div>;
        }
    }
}