import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { Salespersons } from './Salespersons';
import { Products } from './Products';
import { Sales } from './Sales'
import { NewSale } from './NewSale'
import { Customers } from './Customers';
import { Commissions } from './Commissions';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode> 
    <h3>Sales Persons</h3> 
    <Salespersons />

    <h3>Products</h3>
    <Products />

    <h3>Customers</h3>
    <Customers />

    <h3>Sales</h3>
    <Sales />

    <h3>New Sale</h3>
    <NewSale />

    <h3>Yearly Commisions Report</h3>
    <Commissions />
  </React.StrictMode>
);
//<App />
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
